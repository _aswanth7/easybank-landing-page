let toggleBtn = document.getElementById("toggleBtn");
let toggleCloseBtn = document.getElementById("toggleCloseBtn");
let nav = document.getElementById("nav");
const toggleButton = () => {
  nav.classList.toggle("active-nav");
  toggleBtn.classList.toggle("d-none");
  toggleCloseBtn.classList.toggle("d-none");
};
toggleCloseBtn.addEventListener("click", toggleButton);
toggleBtn.addEventListener("click", toggleButton);
